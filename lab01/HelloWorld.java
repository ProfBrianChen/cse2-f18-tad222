//Tal Derei
//CSE 002 Fundamentals of Programming
//August 31, 2018

//Program Purpose: Print "Hello World"

public class HelloWorld {
  public static void main(String[] args) {
    //main method required for every java program
    //Prints Hello World to terminal window
    System.out.println("Hello World");

  } //end of main method
} //end of class

