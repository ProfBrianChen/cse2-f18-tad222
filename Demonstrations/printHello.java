//////////////////////////////////////////////
//Professor Arielle Carr
//Date: August 29, 2018
// How to compile this program:
//    javac printHello.java
// How to run this program:
//    java printHello
//
// Prints "Hello, CSE 002 Students!"
// 
// We refer to these lines as comments - these
// are the lines in a program that DO NOTHING.
// We say that they "aren't executed".  
// But they provide crucial information about 
// the reasoning and intention of the code
// written.  Comments are good programming 
// practice because they make our code 
// readable to others (and even to ourselves).
// It's always better to over-comment than
// to under-comment.  Though, the comments
// provided here are quite lengthy.  While
// you won't be expected to write an essay
// for each line of code, your comments
// should be sufficient enough for a
// person who can't code to understand 
// *what* you are doing (even if they
// don't understand *how* you are doing it)
//
//
// This line defines a class.  We won't study
// classes in this course, but Java requires
// that we have at least one.
// Remember, we usually like to name our 
// classes in a meaningful way.  
public class printHello{ 
  
  // This line defines the main method.
  // The computer begins reading code from
  // the main method. This main method
  // must exist in all programs. 
  public static void main(String[] args){
    
    // This command is a special function
    // called "println" which writes text 
    // to the screen
    System.out.println("Hello, CSE 002 Students!");
    // Note tht we end our command with a 
    // semicolon

  } //This curly brace matches the first
   // one defined after our main method.
   // It ends the method. 
  
}// This curly brace matches the first one
 // defined after our class, Lecture2,
 // and it ends that class 