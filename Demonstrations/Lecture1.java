//////////////////////////////////////////////
// Professor Arielle Carr
// Class demonstration August 27, 2018
// In class demonstration: Building an algorithm 
// Percent Increase Problem: Given two integers
// 		a and b, such that b > a > 0; additionally, 
//		print the percent increase to the screen.
// input: integers a and b
// output: percent increase from a to b 
// percent increase = ((b-a)/a)*100
//
//
// define a class
public class Lecture1{ // Usually, we give our 
				// classes more meaningful names. 
  
  // add main method
  public static void main(String[] args){
		// Declare the variables
		double a;
		double b; 
		double diff;
		double change;
		
		// Assign values to our input variables a and b 
		a = 5;
		b = 9;
	
		// Compute the difference between a and b
		diff = b-a;
		
		// Compute the percent change (specifically, the percent increase) from a to b
		change = (diff/a)*100;
		
		System.out.println("The percent change from a to b is " + change);
 
  }
  
}