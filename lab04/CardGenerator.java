//Tal Derei
//CSE 002 (Fundamentals of Programming)
//September 21, 2018

//Program Card Generator will pick a random card from a standard deck of cards

//Import java.util.Random to incorperate import statements to class and main method
public class CardGenerator {
  //Main method is required for every java program
  public static void main(String[] args) {
    
    int randomRank = (int)(Math.random() * 13 + 1);
    int randomSuit = (int)(Math.random() * 4 + 1);

    System.out.print("Card is ");
    //print out the name of the rank 
		switch(randomRank) {
			case 1:  System.out.print("ace"); 
					break;
      case 2:  System.out.print("2");
					break;
      case 3:  System.out.print("3");
					break;
      case 4:  System.out.print("4");
					break;
      case 5:  System.out.print("5");
					break;
      case 6:  System.out.print("6");
					break;
      case 7:  System.out.print("7");
					break;
      case 8:  System.out.print("8");
					break;
      case 9:  System.out.print("9");
					break;
      case 10:  System.out.print("10");
					break;
			case 11:  System.out.print("jack");
					break;
			case 12:  System.out.print("queen");
					break;
			case 13:  System.out.print("king");
					break;
		}
		
		System.out.print(" of ");
		//print out the name of the suit 
    //use compare operator (==) rather than assignment operator (=) because if and else if are boolean statements 
		if (randomSuit == 1) {
		   System.out.println("clubs.");
    }
    else if (randomSuit == 2) {
		   System.out.println("spades.");
    }
    else if (randomSuit == 3) {
		   System.out.println("hearts.");
    }
    else if (randomSuit == 4) {
		   System.out.println("diamonds.");
    }
    
	 } //end of main methid
} //end of class
    
