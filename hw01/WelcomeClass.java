//Tal Derei
//CSE 002 (Fundamentals of Programming)
//September 04, 2018

//Program Purpose:
  /* displays to the screen a welcome from you, 
  using your Lehigh network ID as a signature */


public class WelcomeClass {
  //main method required for every java program
  public static void main(String[] args) {
  
  //The first 3 lines display a standard introduction message "WELCOME"
  System.out.println("-----------");
  System.out.println("| WELCOME |");
  System.out.println("-----------");
  //The next 5 lines display a unique Lehigh network ID
  System.out.println(" ^  ^  ^  ^  ^  ^");
  System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println  ("<-T--A--D--2--2--2->");
  System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println(" v  v  v  v  v  v");
    
    
  /* tweet-length autobiographic statement: Well, my name is Tal Derei, I'm from Las Vegas,
     and I'm currently a freshman majoring in the CSB Program (Computer Science
     and Business). I'm very exited to be in CSE 002! */
                     
                     
  } //end of main method
} //end of class

    
    