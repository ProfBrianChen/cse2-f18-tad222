//Tal Derei
//CSE 002 (Fundamentals of Programming)
//September 07, 2018

//Program Introduction:
  /* bycicle cyclometer measures speed, distance, etc. and records two kinds of data: 
     time elapsed in seconds and number of rotation of the front wheel during elapsed time*/
  
//import java.text.DecimalFormat to initalize two decimal places after decimal point
import java.text.DecimalFormat;
public class Cyclometer {
  //main method required for every java program
  public static void main(String[] args) {
    
    //our input data
    int secsTrips1  = 480;   //number of seconds for trip 1
    int secsTrips2  = 3220;  //number of seconds for trip 2018
    int countsTrip1 = 1561;  //number of counts (rotations) for trip 1
    int countsTrip2 = 9037;  //number of counts (rotations) for trip 2

    //intermediate variables (useful constants and distances)
    double wheelDiameter = 27.0;                            //diameter of rotating wheel
  	double PI = 3.14159;                                    //constant value of pi
  	double feetPerMile = 5280;                              //number of feet per mile
  	double inchesPerFoot = 12;                              //number of inches per foot
  	double secondsPerMinute = 60;                           //number of seconds per minutes
	  double distanceTrip1 , distanceTrip2 , totalDistance;   //distances in double
    
    //DecimalFormat (declaring and assigning to two decimal places)
    double a1 = (secsTrips1);
    DecimalFormat df1 = new DecimalFormat("0.00");  
    double a2 = (secsTrips2);
    DecimalFormat df2 = new DecimalFormat("0.00");
    double a3 = (countsTrip1);
    DecimalFormat df3 = new DecimalFormat("0.00");
    double a4 = (countsTrip2);
    DecimalFormat df4 = new DecimalFormat("0.00");  
         
    //print the variables with stored input values
    System.out.println("Trip 1 took " + (df1.format(a1 / secondsPerMinute) + " minutes and had " + (df3.format(a3) + " counts."))); //number minutes and counts for trip 1
	  System.out.println("Trip 2 took " + (df2.format(a2 / secondsPerMinute) + " minutes and had " + (df4.format(a4) + " counts."))); //number minutes and countes for trip 2
 
    //calulating total distances using input values
	  distanceTrip1 = countsTrip1 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //distance of trip 1 in miles 
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //distance of trip 2 in miles
	  totalDistance = distanceTrip1 + distanceTrip2; //total distance is distance of trip 1 and distance of trip 2 
    
    //DecimalFormat (declaring and assigning two decimal places)
    double a5 = (distanceTrip1);
    DecimalFormat df5 = new DecimalFormat("0.00");  
    double a6 = (distanceTrip2);
    DecimalFormat df6 = new DecimalFormat("0.00");
    double a7 = (distanceTrip1 + distanceTrip2);
    DecimalFormat df7 = new DecimalFormat("0.00");
    
    //print out the output data
    System.out.println("Trip 1 was " + (df5.format(a5) +" miles"));             //distance (in miles) of trip 1
	  System.out.println("Trip 2 was " + (df6.format(a6) +" miles"));             //distance (in miles) of trip 2
	  System.out.println("The total distance was " + (df7.format(a7) +" miles")); //total combined distance of both trips

  } //end of main method
} //end of class

