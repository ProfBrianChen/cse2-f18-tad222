//Tal Derei
//CSE 002 (Fundamentals of Programming)
//Homework 2
//September 04, 2018

//import java.text.DecimalFormat to initalize two decimal places after decimal point
import java.text.DecimalFormat;
public class Arithmetic { 
  
  //main method required for every java program
  public static void main(String[] args) {
   
      //Assumptions (input variables)
      int numPants  = 3;  //number of pairs of pants
      int numShirts = 2;  //number of sweatshirts
      int numBelts  = 1;  //number of belts
      
      double pantsPrice = 34.98; //cost per pair of pants
      double shirtPrice = 24.99; //cost per shirt 
      double beltCost   = 33.99; //cost per belt
      double paSalesTax = 0.06;  //pa tax rate
    
      //declaring the total cost for each item:
      double totalCostOfPants; 
      double totalCostOfShirts; 
      double totalCostOfBelts; 
    
      totalCostOfPants  = (numPants * pantsPrice * paSalesTax) + (numPants * pantsPrice);    //total cost of pants
      totalCostOfShirts = (numShirts * shirtPrice * paSalesTax) + (numShirts * shirtPrice);  //total cost of shirts
      totalCostOfBelts  = (numBelts * beltCost * paSalesTax) + (numBelts * beltCost);        //total cost of belts
        
      //calculating the total cost of each kind of item (before tax)
      double a1 = (numPants * pantsPrice);
      DecimalFormat df1 = new DecimalFormat("0.00");  
      double a2 = (numShirts * shirtPrice);
      DecimalFormat df2 = new DecimalFormat("0.00");
      double a3 = (numBelts * beltCost);
      DecimalFormat df3 = new DecimalFormat("0.00");
    
      System.out.print("Total cost of each kind of item (before tax):"); 
      System.out.print(" pants = $" + (df1.format(a1)));
      System.out.print(" , shirts = $" + (df2.format(a2)));
      System.out.println(" , belts = $" + (df3.format(a3)));
  
      //calculating the total cost of each kind of item (after tax)
      double a4 = (totalCostOfPants);
      DecimalFormat df4 = new DecimalFormat("0.00");  
      double a5 = (totalCostOfShirts);
      DecimalFormat df5 = new DecimalFormat("0.00");
      double a6 = (totalCostOfBelts);
      DecimalFormat df6 = new DecimalFormat("0.00");
    
      System.out.print("Total cost of each kind of item (after tax):"); 
      System.out.print(" pants = $" + (df4.format(a4)));
      System.out.print(" , shirts = $" + (df5.format(a5)));
      System.out.println(" , belts = $" + (df6.format(a6)));
 
      //calculating the sales tax charged buying all of each kind of item
      double a7 = (numPants * pantsPrice * paSalesTax);
      DecimalFormat df7 = new DecimalFormat("0.00");  
      double a8 = (numShirts * shirtPrice * paSalesTax);
      DecimalFormat df8 = new DecimalFormat("0.00");
      double a9 = (numBelts * beltCost * paSalesTax);
      DecimalFormat df9 = new DecimalFormat("0.00");
    
      System.out.print("Total sales tax charged buying all of each kind of item:"); 
      System.out.print(" pants = $" + (df7.format(a7)));
      System.out.print(" , shirts = $" + (df8.format(a8)));
      System.out.println(" , belts = $" + (df9.format(a9)));
    
      //calculating the total cost of purchases before tax
      double a10 = (((numPants * pantsPrice) + (numShirts * shirtPrice)) + (numBelts * beltCost));
      DecimalFormat df10 = new DecimalFormat("0.00");
      System.out.println("Total cost of purchases before tax: $" + df10.format(a10));
    
      //calculating the total cost of purchases before tax
      double a11 = (((numPants * pantsPrice * paSalesTax) + (numShirts * shirtPrice * paSalesTax)) + (numBelts * beltCost * paSalesTax));
      DecimalFormat df11 = new DecimalFormat("0.00");
      System.out.println("Total sales tax: $" + df11.format(a11)); 
                        
      //calculating the total amount paid for the transaction
      double a12 = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
      DecimalFormat df12 = new DecimalFormat("0.00");
      System.out.println("Total paid for this transaction, including sales tax: $" + df12.format(a12));
    
      //DecimalFormat , Math.floor
        
  } //end of main method
} //end of class
             
         

      
  
  
