//Tal Derei
//CSE 002 (Fundamentals of Programming)
//September 21, 2018

import java.util.Scanner;
import java.util.Random;
  public class CrapsSwitch {
    //Main method is required for every java program
    public static void main(String[] args) {
      //initalize and declare scanner
      Scanner myScanner = new Scanner(System.in);
      
      //prompt message to appear
      System.out.print("Please indicate '1' if you would like to manually enter a pair of dice you want to evaluate. ");
      System.out.println("To randomly cast and evaluate a pair of dice, indicate '2' ");
      
      //declare dice one and dice two as null
      int diceOne = 0;
      int diceTwo = 0;
      
      //prompt user to input either their preference, either input dice or randomly generate dice
      int stateDice = myScanner.nextInt();
      
          //branch that prompts user to enter dice number
          if (stateDice == 1) { //if branch that corresponds to user input "1"
          System.out.println("Enter two dice numbers seperately, one at a time: "); //message prompting user to input two seperate dice rolls, one at a time 
          diceOne = myScanner.nextInt(); //enter first dice roll 
          diceTwo = myScanner.nextInt(); //enter second dice roll
          }
          else { //"else" branch if users inputted number does not correspond with '1'
            diceOne = (int)(Math.random() * 6 + 1); //randomized generated number for first die 
            diceTwo = (int)(Math.random() * 6 + 1); //randomized generated number for second die
          } 
      
      int sumDice = diceOne + diceTwo; //summing the values of the dice to correspond to some string
      String output = ""; //string that corresponds to some sum value of dice
          
          //branch if dice equal each other
          if (diceOne == diceTwo) {
            switch (sumDice) {
              case 2:  output = "Snake Eyes!";
                break;
              case 4:  output = "Hard Four!";
                break;
              case 6:  output = "Hard Six!";
                break;
              case 8:  output = "Hard Eight!";
                break;
              case 10: output = "Hard Ten!";
                break;
              case 12: output = "Boxcars!";
                break;
              default: output = "System Error";
            }
          }
 
         //branch if dice do not equal each other
         else if (diceOne != diceTwo) {
           switch (sumDice) {
              case 3:  output = "Ace Deuce!";
                break;
              case 4:  output = "Easy Four!";
                break;
              case 5:  output = "Fever Five!";
                break;
              case 6:  output = "Easy Six!";
                break;
              case 7: output =  "Seven Out!";
                break;
              case 8: output =  "Easy Eight!";
                break;
              case 9: output =  "Nine!";
                break;
              case 10: output = "Easy Ten!";
                break;
              case 11: output = "Yo-Leven!";
                break; 
             default: output = "System Error";
           }
         }
            System.out.println("You rolled a " + diceOne + " and " + diceTwo + ": " + output); //outputs statement 
            
            
            //I commited the code last night before the 11pm deadline, but the system.out.println statement got deleted.
            //I sent a screenshot of the bitbucket error to professor carr, and she approved that I could add this line  
            //of code, even if the time stamp changes to show after the deadline (refer to CrapsIf.java to display the timestamp
            //of when I first commited both lines of code. Without the system.out.println statement, my program wouldnt output anything.
            
            //Additionally, one of the TA's helped me write this code, and although its supposed to be only switch statements, he said I was
            //able to add if statements due to the way my code functions (comparing repeating and non repeating numbers). 
            
            //This information listed above can be corroborated by professor carr and teaching assistant Christian Cabrera. Thanks!
          

   } //end of main methid
} //end of class