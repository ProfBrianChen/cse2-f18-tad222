//Tal Derei
//CSE 002 (Fundamentals of Programming)
//September 14, 2018

/*Program Function: 
  program that will determine, using input data by the user, the original cost of the check, 
  the percentage tip they wish to pay, number of ways the check will be split amongst the group, 
  and the amount each person in the group needs to spend in order to pay the check in full.*/
  
//import java.util.Scanner to import scanner class
import java.util.Scanner;
public class Check {
  //main method required for every java program
  public static void main(String[] args) {
    
    //declare scanner 
    Scanner myScanner = new Scanner(System.in);
    
    //prompt the user for the input data (original cost of the check)
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    
    //accept user input 
    double checkCost = myScanner.nextDouble();
    
    //prompt user to input user data 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx.xx: "); 
    
    //convert percentage into decimal value
    double tipPercent = myScanner.nextDouble(); 
    tipPercent /= 100;
    
    //prompt the user for the number of people that went to dinner and accept the input
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();

    //declaring and assigning values to values of currency
    double totalCost;
    double costPerPerson;
    int dollars; 
    int dimes;
    int pennies;
    
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson; 
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + (dollars) + '.' + (dimes) + (pennies));              
                      
  } //end of main method
} //end of class
                   


    



    
