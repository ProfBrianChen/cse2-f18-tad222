//Tal Derei
//CSE 002 (Fundamentals of Programming)
//September 17, 2018

//Program should print the volume of a pyramid given dimensions inputted by the user

//import java.util.scanner to import scanner method
import java.util.Scanner;
public class Pyramid {
  //main method required for every java program
  public static void main(String[] args) {

    //declare and initialize scanner
    Scanner myScanner = new Scanner (System.in);
    
    //prompt user to enter pyramid dimensions
    System.out.print("The square side of the pyramid is: ");
    double squareSide = myScanner.nextDouble();
   
    System.out.print("The height of the pyramid is: ");
    double heightSide = myScanner.nextDouble();
    
    //declare and initialize the variables
    double length = squareSide;
    double squareSideNew = Math.pow(squareSide , 2);
    double height = heightSide;
    double volume = (squareSideNew * height)/3;
    
    //Print volume inside pyramid given dimensions inputted by user
    System.out.println("The volume inside the pyramid is: " + volume);
    
  }
}
    