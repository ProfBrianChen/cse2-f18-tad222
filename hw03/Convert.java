//Tal Derei
//CSE 002 (Fundamentals of Programming)
//September 17, 2018

//Program meant to calculate the quanity of rain in cubic miles given the area affected (acres) and average rainfull (inches).

//import java.util.Scanner to import scanner method 
import java.util.Scanner;
public class Convert {
  //main method required for every java program
  public static void main(String[] args) {
    
    //declare scanner 
    Scanner myScanner = new Scanner (System.in);
    
    //prompt user to print amount of area affected
    System.out.print("Enter the affected area in acres: ");
    double areaAmount = myScanner.nextDouble();
    
    //prompt user to print amount of rainfall
    System.out.print("Enter the rainfall in the affected area: ");
    double rainAmount = myScanner.nextDouble();
    
    //declare and initialize variables
    double acres = areaAmount;          //number of acres of land affected (inputed by user)
    double inches = rainAmount;         //average inches of rainfall by hurricane precipitation (inpted by user)
    double acreInches = acres * inches; //volume of rectangular prism with surface area (length * width) equal to one acre by a depth of one inch
    double gallons = 27154.285714286;   //one acre-inch is equal to 27154.285714286 gallons (US liquid)
    double cubicMiles = 9.08169e-13;    //one gallon is equal to 9.08169e-13 cubic miles
   
    //print quanity of rain in cubic miles (converted via dimensional analysis)
    System.out.println((acreInches * gallons * cubicMiles) + " cubic miles ");
    
  }
}
    
    
  
  